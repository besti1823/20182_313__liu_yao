public class Treenode {
    public Treenode left;
    public Treenode right;
    public int Element;

    public Treenode(int element){
        left=null;
        right=null;
        Element=element;
    }

    public void setElement(int element){
        Element=element;
    }

    public int getElement(){
        return  Element;
    }
}