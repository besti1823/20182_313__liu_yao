public class TestStack {
    public static void main(String[] args) {
        ArrayStack arrayStack = new ArrayStack();

        arrayStack.push(20182313);
        arrayStack.push("liuyao");
        arrayStack.push(2313);
        System.out.println("栈：" + arrayStack.toString());
        System.out.println("栈顶：" + arrayStack.peek());
        System.out.println("栈是否为空：" + arrayStack.isEmpty());
        arrayStack.pop();
        System.out.println("弹栈后栈大小：" + arrayStack.size());
        System.out.println("弹栈后栈：" + arrayStack.toString());


    }
}