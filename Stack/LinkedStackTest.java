public class LinkedStackTest {
    public static void main(String[] args) {
        LinkedStack stack = new LinkedStack();
        System.out.println("开始时栈是否为空：" + stack.isEmpty());
        stack.push(20182313);
        stack.push("liuyao");
        System.out.println("栈的个数：" + stack.size());
        System.out.println("栈顶：" + stack.peek());
        stack.pop();
        System.out.println("弹出后输出栈：" + stack.toString());
    }
}