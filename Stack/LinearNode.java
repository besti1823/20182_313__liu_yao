public class LinearNode<T> {
    private LinearNode<T> next;
    private T element;

    public LinearNode(){
        next=null;
        element=null;
    }
    public LinearNode(T ele)
    {
        next = null;
        element = ele;
    }

    public LinearNode<T> getNext(){
        return  next;
    }
    public void setNext(LinearNode node){
        next = node;
    }
    public T getElement(){
        return element;
    }

    public void setElement(T ele) {
        element=ele;
    }
}
