import java.util.Scanner;

public class linbiao {
    public static void main(String[] args) {

        Scanner Input = new Scanner(System.in);

        System.out.println("请输入第1个整数： ");
        int num = Input.nextInt();
        NumNode head = new NumNode(num);

        String a = null;
        int f=1;
        do
        {
            f++;
            System.out.println("请输入第"+f+"个整数： ");
            int num2 = Input.nextInt();

            NumNode x1 = new NumNode(num2);
            add1(head, x1);

            System.out.println("是否继续输入？（y/n）");
            a = Input.nextLine();
            a = Input.nextLine();
        }
        while(a.equalsIgnoreCase("y"));

        NumNode x1 = new NumNode(001);
        NumNode x2 = new NumNode(002);
        NumNode x3 = new NumNode(003);
        NumNode x4 = new NumNode(004);

        System.out.println("添加结点“001”");
        add1(head,x1);
        System.out.println("添加结点“002”");
        add1(head,x2);
        System.out.println("添加结点“003”");
        add1(head,x3);
        System.out.println("添加结点“004”");
        add1(head,x4);

        Printlist(head);
        System.out.println();

        System.out.println("删除结点“003”");
        System.out.println();
        delete(head, x1);

        Printlist(head);

        Sort(head);
        System.out.println();
        System.out.println("排序后结果为：");
        Printlist(head);


    }
    private static class NumNode
    {
        protected NumNode next = null;
        protected int num;
        public NumNode(int num)
        {
            this.num = num;
            next = null;
        }
    }
    public static void Printlist(NumNode Head) {
        NumNode node = Head;
        while (node != null) {
            System.out.print(" " + node.num);
            node = node.next;
        }
    }

    public static void add1(NumNode head, NumNode node)
    {
        if ( head == null)
            head = node;
        else {
            NumNode temp = head;
            while (temp.next != null) {
                temp = temp.next;
            }
            temp.next = node;
        }
    }

    public static NumNode add2(NumNode head, NumNode node)
    {
        node.next = head;
        head = node;
        return head;
    }

    public static void add(NumNode Head, NumNode node1, NumNode node2) {
        NumNode point = Head;
        while ((point.num != node1.num) & point != null) {
            point = point.next;
        }
        if (point.num == node1.num) {
            node1.next = point.next;
            point.next = node2;
        }
    }

    public static void delete(NumNode head, NumNode node) {
        NumNode b = head, c = head;

        while (b != null) {
            if (b.num != node.num) {
                c = b;
                b = b.next;
            } else {
                break;
            }
        }
        c.next = b.next;
        if (b.num != node.num)
            System.out.println("删除失败。原因：未找到节点。");
    }

    public static void  Sort(NumNode head)
    {
        NumNode d = head;
        int temp;

        while (d != null)
        {
            NumNode numNode = d.next;
            while (numNode != null)
            {
                if (numNode.num < d.num)
                {
                    temp = d.num;
                    d.num = numNode.num;
                    numNode.num = temp;
                }
                numNode = numNode.next;
            }
            d = d.next;
        }
    }
}
