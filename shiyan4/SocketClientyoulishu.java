import java.io.*;
import java.net.Socket;

public class SocketClientyoulishu {
    public static void main(String[] args) throws IOException {
        /* 1.连接服务器Socket，接入对应端口*/
        Socket socket = new Socket("192.168.43.25",8801);
//        Socket socket = new Socket("172.16.43.187",8800);

        //2.获得输出流
        OutputStream outputStream = socket.getOutputStream();
        //       PrintWriter printWriter = new PrintWriter(outputStream);
        OutputStreamWriter outputStreamWriter = new OutputStreamWriter(outputStream);
        //获得输入流
        InputStream inputStream = socket.getInputStream();
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream,"UTF-8"));
        //3.输入信息
        String info1 = "1/6+3/4";
//        String info = new String(info1.getBytes("GBK"),"utf-8");
        //     printWriter.write(info);
        //     printWriter.flush();
        outputStreamWriter.write(info1);
        outputStreamWriter.flush();
        socket.shutdownOutput();
        //接收信息
        String reply = null;
        while (!((reply = bufferedReader.readLine()) ==null)){
            System.out.println("Answer is " + reply);
        }
        //4.??????
        bufferedReader.close();
        inputStream.close();
        outputStreamWriter.close();
        //printWriter.close();
        outputStream.close();
        socket.close();
    }
}