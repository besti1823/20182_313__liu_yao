import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

public class Serverfushu {
    public static void main(String[] args) throws IOException {
        //1.建立一个服务器Socket(ServerSocket)绑定指定端口
        ServerSocket serverSocket=new ServerSocket(8801);
        //2.使用accept()方法阻止等待监听，获得新连接
        Socket socket=serverSocket.accept();
        //3.获得输入流
        InputStream inputStream=socket.getInputStream();
        BufferedReader bufferedReader=new BufferedReader(new InputStreamReader(inputStream));
        //获得输出流
        OutputStream outputStream=socket.getOutputStream();
        PrintWriter printWriter=new PrintWriter(outputStream);
        //4.读取用户输入信息
        String info=null;
        System.out.println("Server online......");
        info = bufferedReader.readLine();
        System.out.println("Enter info: " + info);

        //分解数据
        int i,j,k;

        for(i=0,j=0;j<info.length();j++){
            if(!(info.charAt(j)>=48&&info.charAt(j)<=57||info.charAt(j)==46))break;
        }
        double a = Double.parseDouble(info.substring(i,j));

        for(i=j++;j<info.length();j++){
            if(!(info.charAt(j)>=48&&info.charAt(j)<=57||info.charAt(j)==46))break;
        }
        double b = Double.parseDouble(info.substring(i,j));
        if(info.charAt(i-1)=='-')b=-b;

        //运算符
        j++;
        k=j;

        for(i=j++;j<info.length();j++){
            if(!(info.charAt(j)>=48&&info.charAt(j)<=57||info.charAt(j)==46))break;
        }
        double c = Double.parseDouble(info.substring(i,j));

        i=j+1;
        j=info.length()-1;
        double d = Double.parseDouble(info.substring(i,j));
        if(info.charAt(i-1)=='-')d=-d;

        Complex x = new Complex(a,b);
        Complex y = new Complex(c,d);
        //计算并给客户一个响应
        switch ((int)(info.charAt(k))){
            case 43:
                Complex answer1 = x.complexAdd(y);
                String reply1 = answer1.toString();
                printWriter.write(reply1);
                printWriter.flush();
                break;
            case 45:
                Complex answer2 = x.complexSub(y);
                String reply2 = answer2.toString();
                printWriter.write(reply2);
                printWriter.flush();
                break;
            case 42:
                Complex answer3 = x.complexMulti(y);
                String reply3 = answer3.toString();
                printWriter.write(reply3);
                printWriter.flush();
                break;
            case 47:
                Complex answer4 = x.complexDiv(y);
                String reply4 = answer4.toString();
                printWriter.write(reply4);
                printWriter.flush();
                break;
            default:
                String replyf = "ERROR!NOT A OPTION";
                printWriter.write(replyf);
                printWriter.flush();
        }

        //5.关闭资源
        printWriter.close();
        outputStream.close();
        bufferedReader.close();
        inputStream.close();
        socket.close();
        serverSocket.close();
    }
}