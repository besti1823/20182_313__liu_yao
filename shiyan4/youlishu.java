public class youlishu {
    private int fz, fm;

    public youlishu(int numer, int denom) {
        if (denom == 0)
            denom = 1;
        if (denom < 0) {
            denom = -denom;
            numer = -numer;
        }
        fz = numer;
        fm = denom;
        reduce();
    }

    public youlishu() {

    }

    public int getNumerator() {
        return fz;
    }

    public int getDenominator() {
        return fm;
    }

    public youlishu reciprocal() {
        return new youlishu(fm, fz);
    }

    public youlishu add(youlishu op2) {
        int commonDenominator = fm * op2.getDenominator();
        int numerator1 = fz * op2.getDenominator();
        int numerator2 = op2.getNumerator() * fm;
        int sum = numerator1 + numerator2;
        return new youlishu(sum, commonDenominator);
    }

    public youlishu subtract(youlishu op2) {
        int commonDenominator = fm * op2.getDenominator();
        int numerator1 = fz * op2.getDenominator();
        int numerator2 = op2.getNumerator() * fm;
        int difference = numerator1 - numerator2;

        return new youlishu(difference, commonDenominator);
    }

    public youlishu multiply(youlishu op2) {
        int numer = fz * op2.getDenominator();
        int denom = fm * op2.getDenominator();
        return new youlishu(numer, denom);
    }

    public youlishu divide(youlishu op2) {
        return multiply(op2.reciprocal());
    }

    public boolean isLike(youlishu op2) {
        return (fz == op2.getNumerator() && fm == op2.getDenominator());
    }

    @Override
    public String toString() {
        return "youlishu{" +
                fz +
                "/" + fm +
                '}';
    }

    private void reduce() {
        if (fz != 0) {
            int common;
            common = gcd(Math.abs(fz), fm);
            fz = fz / common;
            fm = fm / common;
        }
    }

    private int gcd(int num1, int num2) {
        while (num1 != num2)
            if (num1 > num2)
                num1 = num1 - num2;
            else
                num2 = num2 - num1;
        return num1;
    }

}