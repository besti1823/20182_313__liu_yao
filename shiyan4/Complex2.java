public class Complex2 {
    public static void main(String[] args) {
        Complex a = new Complex(5,-1);
        Complex b = new Complex(3,6);

        System.out.println(a.complexAdd(b));
        System.out.println(a.complexSub(b));
        System.out.println(a.complexMulti(b));
        System.out.println(a.complexDiv(b));
    }
}
