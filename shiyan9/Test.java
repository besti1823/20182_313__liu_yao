import java.util.Stack;

public class Test {

    public static void main(String[] args) {
        Graph graph = new Graph();
        System.out.println("该图的邻接表为：");
        outputGraph(graph);

    }

    /**
     * 输出图的邻接表的方法。
     * @param graph 要输出的图
     */
    public static void outputGraph(Graph graph){
        for (int i=0;i<graph.verNum;i++){
            Vertex vertex = graph.verArray[i];
            System.out.print(vertex.verName);

            Edge current = vertex.edgeLink;
            while (current != null){
                System.out.print("-->"+current.tailName);
                current = current.broEdge;
            }
            System.out.println();
        }

    }

    //    public static void rudu(Graph graph){
//        int[] a = new int[graph.verNum];
//        for(int k=0;k<graph.verNum;k++)
//            a[k]=0;
//        for (int i=0;i<graph.verNum;i++){
//            Vertex vertex = graph.verArray[i];
//            Edge current = vertex.edgeLink;
//            while (current != null){
//                    int m=Integer.parseInt(current.tailName)-1;
//                    a[m]++;
//                current = current.broEdge;
//            }
//        }
//        for (int i=0;i<graph.verNum;i++){
//            System.out.println("v"+(i+1)+": "+a[i]);
//        }
//
//    }
    public static void outputtuota(Graph graph,int[] a){
        Stack<Integer> stack = new Stack<>();
        for (int i=0;i<graph.verNum;i++){
            if(a[i]==0){
                System.out.println(i+1);

            }
        }

    }
}