public class Edge {
    /**
     * 边的尾部节点名称
     */
    String tailName;

    /**
     * 头节点的其他边
     */
    Edge broEdge;
}