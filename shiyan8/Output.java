import java.io.*;

public class Output {
    public static void main(String args[]) throws IOException {
        File file = new File("Output.txt");
        OutputStream outputstream = new FileOutputStream(file);
        BufferedOutputStream bufferedoutputstream = new BufferedOutputStream(outputstream);
        String str = "15\n"+
                "你喜欢看书吗 ?\n"+
                "那你有其他兴趣吗 ?\n"+
                "你喜欢阅读名著吗 ?\n"+
                "那你空闲时会不会感到无聊\n"+
                "你是否能通过你的兴趣丰富自我呢\n"+
                "你想要通过阅读提升自我吗 ?\n"+
                "你将来想要自己写一本书吗 ?\n"+
                "那么你一定是一个内心十分充实的人\n"+
                "你有必要寻找一些兴趣爱好来填补自己的空虚了\n"+
                "那么你可能需要试着更换一下你的兴趣爱好了\n"+
                "非常棒，保持这个兴趣吧\n"+
                "即使只作为单纯的爱好，阅读也可以提升你的人格魅力不是吗 \n"+
                "那么努力阅读吧，相信你可以成功的 !\n"+
                "阅读不应该只是一种娱乐，静下心来，你会发现更多\n"+
                "那么我相信多阅读一些名著会帮助你更容易达成目标\n"+
                "3 7 8\n"+
                "4 9 10\n"+
                "6 11 12\n"+
                "5 13 14\n"+
                "1 3 4\n"+
                "2 5 6\n"+
                "0 1 2\n";
        bufferedoutputstream.write(str.getBytes());
        bufferedoutputstream.flush();
    }
}