
import java.io.File;
import java.io.FileNotFoundException;
import java.util.List;
import java.util.Scanner;

public class Judge{
    private LinkedBinaryTree<String> tree;

    public Judge(String filename) throws FileNotFoundException {

        File inputFile = new File(filename);
        Scanner scan = new Scanner(inputFile);

        int numberNodes = scan.nextInt();

        scan.nextLine();
        int root = 0;
        int left, right;

        List<LinkedBinaryTree<String>> nodes = new java.util.ArrayList<LinkedBinaryTree<String>>();

        for (int i = 0; i < numberNodes; i++)
            nodes.add(i, new LinkedBinaryTree<String>(scan.nextLine()));

        while (scan.hasNext()) {
            root = scan.nextInt();
            left = scan.nextInt();
            right = scan.nextInt();
            //  scan.nextLine();

            nodes.set(
                    root,
                    new LinkedBinaryTree<String>((nodes.get(root))
                            .getRootElement(), nodes.get(left), nodes
                            .get(right)));
        }
        tree = nodes.get(root);

    }

    /**
     * Follows the decision tree based on user responses.
     */
    public void evaluate() {
        LinkedBinaryTree<String> current = tree;
        Scanner scan = new Scanner(System.in);

        while (current.size() > 1) {
            System.out.println(current.getRootElement());
            if (scan.nextLine().equalsIgnoreCase("N"))
                current = current.getLeft();
            else
                current = current.getRight();
        }

        System.out.println("Output the Judge Result :" + current.getRootElement());
    }

    public int countLeaf() {//递归
        return countLeaf2(tree.root);
    }

    public int countLeaf2(BinaryTreeNode count) {
        int cou = 0;
        int num = 0;
        if (count == null) {
            return 0;
        }
        if (count.getLeft() == null && count.getRight() == null) {
            return 1;
        }
        if (count.getLeft() != null || count.getRight() != null) {
            cou = countLeaf2(count.getLeft());
            num = countLeaf2(count.getRight());
        }
        return cou + num;

    }

    public int deepth() {
        return deepth2(tree.root);
    }

    public int deepth2(BinaryTreeNode<String> deep) {
        if (deep == null) {
            return 0;
        }
        int left = deepth2(deep.left);
        int right = deepth2(deep.right);
        return Math.max(left, right) + 1;
    }

}