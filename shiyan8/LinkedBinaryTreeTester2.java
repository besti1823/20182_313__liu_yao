public class LinkedBinaryTreeTester2
{
    public static void main(String[] args) {
        String[] inorder = new String[]{"H","D","I","B","E","M","J","N","A","F","C","K","G","L"};//中序
        String[] preorder = new String[]{"A","B","D","H","I","E","J","M","N","C","F","G","K","L"};//先序

        LinkedBinaryTree2 tree = new LinkedBinaryTree2();

        tree.RebuildTree(inorder,preorder);
        System.out.println(tree.toString());
    }
}