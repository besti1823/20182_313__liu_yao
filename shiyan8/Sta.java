import shiyan888.EmptyCollectionException;

public interface Sta<T> {
    public void push(T element);
    public T pop() throws EmptyCollectionException;
    public T peek() throws EmptyCollectionException, shiyan888.EmptyCollectionException;
    public boolean isEmpty();
    public int size() throws EmptyCollectionException;
    public String toString();
}