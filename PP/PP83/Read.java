package PP83;

public abstract class Read {
    private String pages;
    private String chara;

    public Read(String pages, String chara) {
        this.pages = pages;
        this.chara = chara;
    }

    public String getPages() {
        return pages;
    }

    public void setPages(String pages) {
        this.pages = pages;
    }

    public String getChara() {
        return chara;
    }

    public void setChara(String chara) {
        this.chara = chara;
    }

    @Override
    public String toString() {
        return "Read{" +
                "pages='" + pages + '\'' +
                ", chara='" + chara + '\'' +
                '}';
    }

    public abstract void show();
}