package PP83;

public class Reads {
    private Read[] reads;

    public Reads() {
        reads = new Read[2];
        reads[0] = new Book("350", "knowledge");
        reads[1] = new Magazine("100", "entertainment");
    }

    public void introduceall() {
        for (Read read : reads) {
            System.out.println(read);
            read.show();
        }
    }
}