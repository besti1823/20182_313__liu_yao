package PP87;

public abstract class Electronic {
    private String weight;
    private String price;

    public Electronic(String weight, String price) {
        this.weight = weight;
        this.price = price;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "Electronic{" +
                "weight='" + weight + '\'' +
                ", price='" + price + '\'' +
                '}';
    }

    public abstract void show();
}