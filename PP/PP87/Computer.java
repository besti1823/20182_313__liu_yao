package PP87;

public class Computer extends Electronic {
    public Computer(String weight, String price) {
        super(weight, price);
    }

    @Override
    public void show() {
        System.out.println("Computer!");
    }
}