package PP87;

public class Phone extends Electronic {
    public Phone(String weight, String price) {
        super(weight, price);
    }

    @Override
    public void show() {
        System.out.println("Phone!");
    }
}