package PP87;

public class Electronics {
    private Electronic[] electronics;

    public Electronics(){
        electronics = new Electronic[2];
        electronics[0] = new Computer("20kg","$9000");
        electronics[1] = new Phone("2kg","$4000");
    }

    public void showall() {
        for (Electronic electronic : electronics) {
            System.out.println(electronic);
            electronic.show();
        }
    }
}
