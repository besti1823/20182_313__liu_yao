package PP84;

public abstract class Sport {
    private String func;
    private String name;

    public Sport(String func, String name) {
        this.func = func;
        this.name = name;
    }

    public String getFunc() {
        return func;
    }

    public void setFunc(String func) {
        this.func = func;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Sport{" +
                "func='" + func + '\'' +
                ", name='" + name + '\'' +
                '}';
    }

    public abstract void exhibit();
}