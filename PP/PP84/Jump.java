package PP84;

public class Jump extends Sport {
    public Jump(String func, String name) {
        super(func, name);
    }

    @Override
    public void exhibit() {
        System.out.println("High!");
    }
}