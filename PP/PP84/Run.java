package PP84;

public class Run extends Sport {
    public Run(String func, String name) {
        super(func, name);
    }

    @Override
    public void exhibit() {
        System.out.println("Fast!");
    }
}