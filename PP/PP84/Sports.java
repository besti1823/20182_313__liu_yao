package PP84;

public class Sports {
    private Sport[] sports;

    public Sports() {
        sports = new Sport[2];
        sports[0] = new Run("350", "knowledge");
        sports[1] = new Jump("100", "entertainment");
    }

    public void exhibitall() {
        for (Sport sport : sports) {
            System.out.println(sport);
            sport.exhibit();
        }
    }
}