public class Book
{
 private String bookname;
 private String author;
 private String press;
 private String copyrightdate;
 public Book()
 {
  String bookname="bookname1";
  String author="author1";
  String press="press1";
  String copyrightdate="copyrightdate1";
 }
 public void setbookname(String bookname2)
 {
  bookname=bookname2;
 }
 public void setauthor(String author2)
 {
  author=author2;
 }
 public void setpress(String press2)
 {
  press=press2;
 }
 public void setcopyrightdate(String copyrightdate2)
 {
  copyrightdate=copyrightdate2;
 }
 public String getbookname()
 {
  return bookname;
 }
 public String getauthor()
 {
  return author;
 }
 public String getpress()
 {
  return press;
 }
 public String getcopyrightdate()
 {
  return copyrightdate;
 }
 public String toString()
 {
  return ("Bookname:"+bookname+"\n"+"Author:"+author+"\n"+"Press:"+press+"\n"+"Copyrightdate:"+copyrightdate+"\n");
 }
}
