package PP967;

import java.util.Collection;
import java.util.Collections;
import java.util.Scanner;

public class PP9672 implements PP9671,Comparable<PP9672> {
    private int priority;
    private String name;

    public PP9672(String name) {
        this.name = name;
    }

    @Override
    public void setPriority(int n) {
        this.priority=n;
    }

    @Override
    public int getPriority() {
        return priority;
    }

    public void set(PP9672[] t,int[] n){
        for(int i=0;i<t.length;i++){
            t[i].setPP9671(n[i]);
        }
    }

    public String getName() {
        return this.name;
    }

    @Override
    public int compareTo(PP9672 c) {
        if(this.priority>c.PP9671)
            return 1;
        else if(this.priority<c.pp9671)
            return -1;
        else
            return 0;
    }

}