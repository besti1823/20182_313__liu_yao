package PP967;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class PP9673 {
    public static void main(String[] args) {
        PP9672[] list=new PP9672[4];
        int[] a=new int[4];
        List<PP9672> tasklist=new ArrayList<>();

        PP9672 task=new PP9672("");
        list[0]=new PP9672("Eat");
        list[1]=new PP9672("Study");
        list[2]=new PP9672("Run");
        list[3]=new PP9672("Java");

        for(int i=0;i<4;i++){
            tasklist.add(list[i]);
        }

        Scanner in=new Scanner(System.in);
        System.out.println("请输入一组优先级：(数字越大优先级越高）");
        for(int i=0;i<4;i++){
            a[i]=in.nextInt();
        }

        task.set(list,a);
        for(PP9672 c:list){
            System.out.println(c.getName()+",优先级:"+c.getPriority());
        }

    }
}