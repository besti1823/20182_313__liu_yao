package PP1023;

import java.util.Scanner;

public class PP10232 {
    public static void main(String[] args) {
        Scanner in=new Scanner(System.in);
        String a=null;
        do{
            try{
                System.out.println("Enter a string (DONE to stop):");
                a=in.nextLine();
                if(a.length()>=20){
                    throw new PP10233("键入过多字符！");
                }
                else System.out.println("你输入的是："+a);
            }
            catch (PP10233 e){
                System.out.println("Too much message!");
            }
        }while(!a.equals("DONE"));
    }
}