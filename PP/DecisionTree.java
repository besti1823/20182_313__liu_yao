import java.util.Scanner;
import shiyan888.*;
public class DecisionTree {
    public static void main(String[] args) throws EmptyCollectionException {
        LinkedBinaryTree<String> ques31 = new LinkedBinaryTree<String>("乒乓球");
        LinkedBinaryTree<String> ques30 = new LinkedBinaryTree<String>("篮球");
        LinkedBinaryTree<String> ques29 = new LinkedBinaryTree<String>("足球");
        LinkedBinaryTree<String> ques28 = new LinkedBinaryTree<String>("羽毛球");
        LinkedBinaryTree<String> ques27 = new LinkedBinaryTree<String>("网球");
        LinkedBinaryTree<String> ques26 = new LinkedBinaryTree<String>("保龄球");
        LinkedBinaryTree<String> ques25 = new LinkedBinaryTree<String>("排球");
        LinkedBinaryTree<String> ques24 = new LinkedBinaryTree<String>("三维弹球");
        LinkedBinaryTree<String> ques23 = new LinkedBinaryTree<String>("桌球");
        LinkedBinaryTree<String> ques22 = new LinkedBinaryTree<String>("台球");
        LinkedBinaryTree<String> ques21 = new LinkedBinaryTree<String>("气球");
        LinkedBinaryTree<String> ques20 = new LinkedBinaryTree<String>("健身球");
        LinkedBinaryTree<String> ques19 = new LinkedBinaryTree<String>("悠悠球");
        LinkedBinaryTree<String> ques18 = new LinkedBinaryTree<String>("地球");
        LinkedBinaryTree<String> ques17 = new LinkedBinaryTree<String>("高尔夫球");
        LinkedBinaryTree<String> ques16 = new LinkedBinaryTree<String>("橄榄球");

        LinkedBinaryTree<String> ques15 = new LinkedBinaryTree<String>("用手？", ques30, ques31);
        LinkedBinaryTree<String> ques14 = new LinkedBinaryTree<String>("用脚？", ques28, ques29);
        LinkedBinaryTree<String> ques13 = new LinkedBinaryTree<String>("用工具？", ques26, ques27);
        LinkedBinaryTree<String> ques12 = new LinkedBinaryTree<String>("充气？", ques24, ques25);
        LinkedBinaryTree<String> ques11 = new LinkedBinaryTree<String>("双人？", ques22, ques23);
        LinkedBinaryTree<String> ques10 = new LinkedBinaryTree<String>("有实体？", ques20, ques21);
        LinkedBinaryTree<String> ques9 = new LinkedBinaryTree<String>("竞技性？", ques18, ques19);
        LinkedBinaryTree<String> ques8 = new LinkedBinaryTree<String>("能吃？", ques16, ques17);

        LinkedBinaryTree<String> ques7 = new LinkedBinaryTree<String>("运动？", ques14, ques15);
        LinkedBinaryTree<String> ques6 = new LinkedBinaryTree<String>("要钱？", ques12, ques13);
        LinkedBinaryTree<String> ques5 = new LinkedBinaryTree<String>("累？", ques10, ques11);
        LinkedBinaryTree<String> ques4 = new LinkedBinaryTree<String>("有气？", ques8, ques9);

        LinkedBinaryTree<String> ques3 = new LinkedBinaryTree<String>("历史悠久？", ques6, ques7);
        LinkedBinaryTree<String> ques2 = new LinkedBinaryTree<String>("透光？", ques4, ques5);

        LinkedBinaryTree<String> ques1 = new LinkedBinaryTree<String>("吃的？", ques2, ques3);

        LinkedBinaryTree<String> quesPrint = ques1;
        Scanner scan = new Scanner(System.in);
        String rpy;
//        boolean brk = false;

//        String able1 = "Y";
//        String able2 = "y";
//        String able3 = "N";
//        String able4 = "n";

        for(;;){
            System.out.print(quesPrint.getRootElement());
            if(quesPrint.root.left == null && quesPrint.root.right == null)
                break;

            rpy = scan.nextLine();
            for (;;){
                if(rpy.compareTo("y") == 0 || rpy.compareTo("Y") == 0 || rpy.compareTo("n") == 0 || rpy .compareTo("N") == 0)
                    break;
                else
                    System.out.println("错误，重新输入（Y/y，N/n）：");
            }
            if(rpy.compareTo("y") == 0 || rpy.compareTo("Y") == 0 )
                quesPrint = quesPrint.getLeft();
            if(rpy.compareTo("n") == 0 || rpy .compareTo("N") == 0)
                quesPrint = quesPrint.getRight();
        }
    }
}