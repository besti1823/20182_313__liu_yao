import shiyan888.*;

public class LinkedBinarySearchTree<T extends Comparable<T>>
        extends LinkedBinaryTree<T> implements BinarySearchTree<T>
{
    //-----------------------------------------------------------------
// Creates an empty binary search tree.
//-----------------------------------------------------------------
    public LinkedBinarySearchTree()
    {
        super();
    }
    //-----------------------------------------------------------------
// Creates a binary search tree with the specified element at its
// root.
//-----------------------------------------------------------------
    public LinkedBinarySearchTree (T element)
    {
        root = new BSTNode<T>(element);
    }
    //-----------------------------------------------------------------
// Adds the specified element to this binary search tree.
//-----------------------------------------------------------------
    public void add (T item)
    {
        if (root == null)
            root = new BSTNode<T>(item);
        else
            ((BSTNode)root).add(item);
    }
    //-----------------------------------------------------------------
// Removes and returns the element matching the specified target
// from this binary search tree. Throws an ElementNotFoundException
// if the target is not found.
//-----------------------------------------------------------------
    public T remove (T target) throws ElementNotFoundException {
        BSTNode<T> node = null;
        if (root != null)
            node = ((BSTNode)root).find(target);
        if (node == null)
            throw new ElementNotFoundException ("Remove operation failed. "
                    + "No such element in tree.");
        root = ((BSTNode)root).remove(target);
        return node.getElement();
    }

    public T findMin() throws ElementNotFoundException {
        BSTNode<T> node = null;
        if (root != null)
            node = (BSTNode)root;
        if (node == null)
            throw new ElementNotFoundException ("Remove operation failed. "
                    + "No such element in tree.");
        while (node.getLeft() != null)
            node = (BSTNode)node.getLeft();
        return node.getElement();
    }

    public T findMax() throws ElementNotFoundException {
        BSTNode<T> node = null;
        if (root != null)
            node = (BSTNode)root;
        if (node == null)
            throw new ElementNotFoundException ("Remove operation failed. "
                    + "No such element in tree.");
        while (node.getLeft() != null)
            node = (BSTNode)node.getRight();
        return node.getElement();
    }
}