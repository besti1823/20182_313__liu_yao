import java.util.Scanner;
import java.text.DecimalFormat;;
public class PP35
{
 public static void main(String[] args)
 {
  Scanner scan=new Scanner(System.in);
  DecimalFormat fmt = new DecimalFormat("0.####");
  double r,v,s;
  System.out.print("Enter r : ");
  r=scan.nextDouble();
  v=4.0/3.0*Math.PI*r;
  s=4*Math.PI*r*r;
  System.out.println("V = " + fmt.format(v));
  System.out.println("S = " + fmt.format(s));
 }
}
