package PP82;

public abstract class Employer {
    private String position;
    private String job;

    public Employer(String position, String job) {
        this.position = position;
        this.job = job;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getJob() {
        return job;
    }

    public void setJob(String job) {
        this.job = job;
    }

    @Override
    public String toString() {
        return "Employer{" +
                "position='" + position + '\'' +
                ", job='" + job + '\'' +
                '}';
    }

    public abstract void introduce();

}