package PP82;

public class Nurse extends Employer {
    public Nurse(String position, String job) {
        super(position, job);
    }

    @Override
    public void introduce() {
        System.out.println("I take care of people.");
    }
}