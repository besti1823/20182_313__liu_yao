package PP82;

public class Hospital {
    private Employer[] employers;

    public Hospital() {
        employers = new Employer[2];
        employers[0] = new Doctor("Doctor", "cure");
        employers[1] = new Nurse("Nurse", "care");
    }

    public void introduceall() {
        for (Employer employer : employers) {
            System.out.println(employer);
            employer.introduce();
        }
    }
}