package PP82;

public class Doctor extends Employer {
    public Doctor(String position, String job) {
        super(position, job);
    }

    @Override
    public void introduce() {
        System.out.println("I cure people.");
    }
}