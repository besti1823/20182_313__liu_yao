package PP92;

public class PP922 extends PP921
{
    private double bonus;
    //-----------------------------------------------------------------
    // Constructor: Sets up this executive with the specified
    // information.
    //-----------------------------------------------------------------
    public PP922(String eName,String eAddtress,String ePhone,String socSecNumber,int day)
    {
        super (eName,eAddtress,ePhone,socSecNumber,day );
        bonus = 0;
    }
    //-----------------------------------------------------------------
    // Awards the specified bonus to this executive.
    //-----------------------------------------------------------------
    public void awardBonus (double execBonus)
    {
        bonus = execBonus;
    }
    //-----------------------------------------------------------------
    // Computes and returns the pay for an executive, which is the
    // regular employee payment plus a one-time bonus.
    //-----------------------------------------------------------------
    public int holiday(){
        return 2;
    }
}