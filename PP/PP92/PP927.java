package PP92;

public class PP927
{
    private PP926[] staffList;
    //-----------------------------------------------------------------
    // Constructor: Sets up the list of staff members.
    //-----------------------------------------------------------------
    public PP927 ()
    {
        staffList = new PP926[6];
        staffList[0] = new PP922 ("Tony", "123 Main Line",
                "555-0469", "123-45-6789", 1);
        staffList[1] = new PP921 ("Paulie", "456 Off Line",
                "555-0101", "987-65-4321", 3);
        staffList[2] = new PP921 ("Vito", "789 Off Rocker",
                "555-0000", "010-20-3040", 2);
        staffList[3] = new PP924 ("Michael", "678 Fifth Ave.",
                "555-0690", "958-47-3625", 4);
        staffList[4] = new PP926 ("Adrianna", "987 Babe Blvd.",
                "555-8374");
        staffList[5] = new PP926 ("Benny", "321 Dud Lane",
                "555-7282");
        ((PP922)staffList[0]).awardBonus (500.00);
        ((PP924)staffList[3]).addHours (40);
    }
    //-----------------------------------------------------------------
    // Pays all staff members.
    //-----------------------------------------------------------------
    public void Holiday()
    {
        double day;
        for(int count = 0;count<staffList.length;count++)
        {
            System.out.println(staffList[count]);
            day = staffList[count].Holiday();
            if(day == 0)
                System.out.println("Please go on keeping your passion!");
            else
                System.out.println("Holiday: "+ (int)day + " day");
            System.out.println("--------------------------------------");
        }
    }
}
