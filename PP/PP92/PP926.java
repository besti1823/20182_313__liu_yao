package PP92;

public class PP926 extends PP925
{
    //-----------------------------------------------------------------
    // Constructor: Sets up this volunteer using the specified
    // information.
    //-----------------------------------------------------------------
    public PP926 (String eName, String eAddress, String ePhone)
    {
        super (eName, eAddress, ePhone);
    }
    //-----------------------------------------------------------------
    // Returns a zero pay value for this volunteer.
    //-----------------------------------------------------------------
    public int Holiday()
    {
        return 0;
    }
}