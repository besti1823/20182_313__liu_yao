package PP92;

public class PP921 extends PP926
{
    protected String socialSecurityNumber;
    protected int Holiday;
    //-----------------------------------------------------------------
    // Constructor: Sets up this employee with the specified
    // information.
    //-----------------------------------------------------------------
    public PP921 (String eName, String eAddress, String ePhone,
                      String socSecNumber, int day)
    {
        super (eName, eAddress, ePhone);
        socialSecurityNumber = socSecNumber;
        Holiday = day;
    }
    //-----------------------------------------------------------------
    // Returns information about an employee as a string.
    //-----------------------------------------------------------------
    public String toString()
    {
        String result = super.toString();
        result += "\nSocial Security Number: " + socialSecurityNumber;
        return result;
    }
    //-----------------------------------------------------------------
    // Returns the holiday for this employee.
    //-----------------------------------------------------------------
    public int Holiday()
    {
        return Holiday;
    }
}