package PP92;

public class PP924 extends PP921
{
    private int hoursWorked;
    //-----------------------------------------------------------------
    // Constructor: Sets up this hourly employee using the specified
    // information.
    //-----------------------------------------------------------------
    public PP924 (String eName, String eAddress, String ePhone,
                    String socSecNumber, int day)
    {
        super (eName, eAddress, ePhone, socSecNumber, day);
        hoursWorked = 0;
    }
    //-----------------------------------------------------------------
    // Adds the specified number of hours to this employee's
    // accumulated hours.
    //-----------------------------------------------------------------
    public void addHours (int moreHours)
    {
        hoursWorked += moreHours;
    }
    //-----------------------------------------------------------------
    // Computes and returns the pay for this hourly employee.
    //-----------------------------------------------------------------
    public int holiday()
    {
        return 0;
    }
    //-----------------------------------------------------------------
    // Returns information about this hourly employee as a string.
    //-----------------------------------------------------------------
    public String toString()
    {
        String result = super.toString();
        result += "\nCurrent hours: " + hoursWorked;
        //count--;
        return result;
    }
}