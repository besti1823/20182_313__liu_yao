package PP86;

import java.util.Scanner;

public class Rectangle extends Figures {
    public Rectangle(String circum, String area) {
        super(circum, area);
    }

    @Override
    public void change() {
        Scanner scan = new Scanner(System.in);
        double circum = scan.nextDouble();
        double area = scan.nextDouble();
        System.out.println("Circum:"+circum+"\n"+"Area:"+area);
    }
}
