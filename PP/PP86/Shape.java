package PP86;

public class Shape {
    private Figures[] figures;

    public Shape(){
        figures = new Figures[2];
        figures[0] = new Triangle("10cm","20cm^2");
        figures[1] = new Rectangle("20cm","40cm^2");
    }

    public void changeall(){
        System.out.println("Input your new Circumference and Area:");
        for (Figures population : figures) {
            population.change();
        }
    }
}
