package PP86;

import java.util.Scanner;

public class Triangle extends Figures{
    public Triangle(String circum, String area) {
        super(circum, area);
    }

    @Override
    public void change() {
        Scanner scan = new Scanner(System.in);
        double circum = scan.nextDouble();
        double area = scan.nextDouble();
        System.out.println("Circum:"+circum+"\n"+"Area:"+area);
    }
}