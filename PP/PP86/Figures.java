package PP86;

public abstract class Figures {
    private String circum;
    private String area;

    public Figures(String circum, String area) {
        this.circum = circum;
        this.area = area;
    }

    public String getCircum() {
        return circum;
    }

    public void setCircum(String circum) {
        this.circum = circum;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    @Override
    public String toString() {
        return "Figures{" +
                "circum='" + circum + '\'' +
                ", area='" + area + '\'' +
                '}';
    }

    public abstract void change();
}