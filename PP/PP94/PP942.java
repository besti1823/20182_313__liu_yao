package PP94;

public class PP942
{
    public static void main(String[] args)
    {
        Secret hush = new Secret("Hello World");
        PP941 pass = new PP941("Java chengxu sheji is the best！",6);
        System.out.println("明文： " + hush);

        hush.encrypt();
        System.out.println("密文： " + hush);

        hush.decrypt();
        System.out.println("解密： " + hush);

        System.out.println(pass);
        pass.encrypt();
        System.out.println("Password： " + pass);
    }
}