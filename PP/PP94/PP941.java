package PP94;

public class PP941 implements Encryptable
{
    private String str1,str2;
    private int k;

    public PP941(String str1, int k)
    {
        this.str1 = str1;
        this.k = k;
    }

    @Override
    public String encrypt()
    {
        String str2 = "";
        for (int i=0; i<str1.length(); i++)
        {
            char c = str1.charAt(i);
            c += k%26;
            str2 += c;
        }
        System.out.println("加密前：" + str1);
        System.out.println("加密后为：" + str2);
        return str2;
    }

    public String decrypt()
    {
        return null;
    }
}