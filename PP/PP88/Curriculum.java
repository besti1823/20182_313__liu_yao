package PP88;

public abstract class Curriculum {
    private String name;
    private String time;

    public Curriculum(String name, String time) {
        this.name = name;
        this.time = time;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    @Override
    public String toString() {
        return "Curriculum{" +
                "name='" + name + '\'' +
                ", time='" + time + '\'' +
                '}';
    }

    public abstract void show();
}
