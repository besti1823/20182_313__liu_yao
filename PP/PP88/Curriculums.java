package PP88;

public class Curriculums {
    private Curriculum[] curriculums;

    public Curriculums(){
        curriculums = new Curriculum[2];
        curriculums[0] = new Math("Math","40min");
        curriculums[1] = new English("English","40min");
    }

    public void showall() {
        for (Curriculum curriculum : curriculums) {
            System.out.println(curriculum);
            curriculum.show();
        }
    }
}