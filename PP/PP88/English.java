package PP88;

public class English extends Curriculum {
    public English(String name, String time) {
        super(name, time);
    }

    @Override
    public void show() {
        System.out.println("English!");
    }
}