package PP85;

public class Information {
    private Population[] populations;

    public Information(){
        populations = new Population[2];
        populations[0] = new Age();
        populations[1] = new Nationality();
    }

    public void showall(){
        System.out.println("Input your age and nationality:");
        for (Population population : populations) {
            population.show();
        }
    }
}