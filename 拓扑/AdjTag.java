

public class AdjTag {
    private LinearNode<Integer>[] node;

    public AdjTag(int n)
    {
        node = new LinearNode[n];
    }

    public void insert(int head,int wight,int v)
    {
        if(node[v] == null)
        {
            node[v] = new LinearNode<Integer>();
            node[v].setElement(head);
            node[v].setNote(wight);
        }
        else
        {
            LinearNode<Integer> c = new LinearNode<Integer>();
            c.setNote(wight);
            c.setElement(head);
            node[v].setNext(c);
        }
    }
    public int du(int n)
    {
        if(node[n].getNext()==null)
        {
            return 1;
        }
        else  return 2;
    }
    public String tostring(int n)
    {
        String result = "";
        if (node[n].getNext()!=null)
        {
            result = result+node[n].getElement()+"|"+node[n].getNote()+" -->"+node[n].getNext().getElement()+"|"+node[n].getNext().getNote();

        }
        else
        {
            result = result+node[n].getElement()+"|"+node[n].getNote();
        }
        return  result;
    }
}