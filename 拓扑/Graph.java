

import java.util.ArrayList;

public class Graph {
    public ArrayList<String> vertexList;
    public int[][] edges;
    public int numofEdges;
    public int n;

    public Graph(int n) {
        vertexList = new ArrayList<String>(n);
        edges = new int[n][n];
        numofEdges = 0;
        this.n = n;
    }

    public int getNumofVertex() {
        return vertexList.size();
    }

    public int getNunofEdges() {
        return numofEdges;
    }

    public int getWeight(String temp1, String temp2) {
        int i = vertexList.indexOf(temp1);
        int j = vertexList.indexOf(temp2);
        return edges[i][j];
    }

    public void InsertVertex(String vertex) {
        vertexList.add(vertex);
    }

    public void RemoveVertex(String vertex){
        vertexList.remove(vertex);
    }

    public void InsertEdge(int v1, int v2, int weight) {
        edges[v1][v2] = weight;
        edges[v2][v1] = weight;
        numofEdges++;
    }

    public void deleteEdge(int v1, int v2) {
        edges[v1][v2] = 0;
        numofEdges--;
    }

    public int wuxiangtu(int a) {
        int result = 0;
        for (int i = 0; i < n; i++) {
            result += edges[a][i];
        }
        return result;
    }

    public void InsertEdge2(int v1, int v2, int weight) {
        edges[v1][v2] = weight;
        numofEdges++;
    }

    public int youxiangtu(int a)
    {
        int result = 0;
        for (int i = 0; i < n; i++) {
            result += edges[a][i];
        }
        for(int i=0;i<n;i++)
        {
            result +=edges[i][a];
        }
        return  result;
    }
}