import java.io.*;

public class FileTest1 {
    public static void main(String[] args) throws IOException {
        //（1）文件创建（文件类实例化）
        File file = new File("C:\\Users\\dell\\IdeaProjects\\MyUtil\\src", "HelloWorld.txt");
        //File file = new File("HelloWorld.txt");
//        File file1 = new File("C:\\Users\\besti\\Desktop\\FileTest\\Test\\Test");
//        file1.mkdir();
//        file1.mkdirs();

        if (!file.exists()) {
            file.createNewFile();
        }
        // file.delete();
        //（2）文件读写
        //第一种：字节流读写，先写后读
        OutputStream outputStream1 = new FileOutputStream(file);
        byte[] hello = {'H', 'e', 'l', 'l', 'o', ',', 'L', 'y', '!'};
        outputStream1.write(hello);
        outputStream1.flush();//可有可无，不执行任何操作！！！

        InputStream inputStream1 = new FileInputStream(file);
        while (inputStream1.available() > 0) {
            System.out.print((char) inputStream1.read() + "  ");
        }
        inputStream1.close();
        System.out.println("\n文件读写结束：OutputStream、InputStream先写后读");
    }
}
