import java.io.*;


public class FileTest4 {
    public static void main(String[] args) throws IOException {
        //（1）文件创建（文件类实例化）
        File file = new File("C:\\Users\\dell\\IdeaProjects\\MyUtil\\src", "HelloWorld.txt");
        //File file = new File("HelloWorld.txt");
//        File file1 = new File("C:\\Users\\besti\\Desktop\\FileTest\\Test\\Test");
//        file1.mkdir();
//        file1.mkdirs();

        if (!file.exists()) {
            file.createNewFile();
        }
        // file.delete();
        //（2）文件读写
        //第二种：字符流读写，先写后读(两种读)
        Writer writer2 = new FileWriter(file);
        BufferedWriter bufferedWriter = new BufferedWriter(writer2);
        String content3 = "使用bufferedWriter写入：Hello Ly";
        bufferedWriter.write(content3, 0, content3.length());
        bufferedWriter.flush();
        bufferedWriter.close();

        String content = "";
        Reader reader2 = new FileReader(file);
        BufferedReader bufferedReader = new BufferedReader(reader2);
        System.out.println("\n下面是用BufferedReader读出的数据：");
        while ((content = bufferedReader.readLine()) != null) {
            System.out.println(content);
        }
    }
}