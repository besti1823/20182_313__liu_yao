import java.io.*;
import java.util.Scanner;

/**
 * ================================================================
 * Copyright (C) 2019 20182324-yyh All rights reserved.
 * Created with IntelliJ IDEA.
 * User: 20182324-yyh
 * Name: ComplexIO.java
 * Time: 2019-10-13 16:53:25
 * Description:
 * ================================================================
 */
public class ComPlex
{
    public static void main(String[] args) throws IOException
    {
        //（1）文件创建（文件类实例化）
        File file = new File("C:\\Users\\dell\\IdeaProjects\\FileTest\\src2+", "ComplexIO.txt");

        if (!file.exists()) {
            file.createNewFile();
        }
        //（2）文件读写
        String s = "", s1 = "";
        Scanner scan = new Scanner(System.in);
        System.out.print("请输入第一个复数：");
        s = scan.nextLine();
        System.out.print("请输入第二个复数：");
        s1 = scan.nextLine();

        Writer writer2 = new FileWriter(file);
        writer2.write(s);
        writer2.flush();
        writer2.append(" "+s1);
        writer2.flush();

        Reader reader2 = new FileReader(file);
        System.out.println("用Reader读出输入的两个复数：");
        while (reader2.ready()) {
            System.out.print((char) reader2.read() + "");
        }

        int i,j,flag=1,flag1=1;
        for (i=0,j=0;j<s.length();j++)
        {
            if (!(s.charAt(j)>=48&&s.charAt(j)<=57||s.charAt(j)==46))
            {
                break;
            }
        }
        double a = Double.parseDouble(s.substring(i,j));

        for (i=j++;j<s.length();j++)
        {
            if (!(s.charAt(j)>=48&&s.charAt(j)<=57||s.charAt(j)==46))
            {
                break;
            }
        }
        double b = Double.parseDouble(s.substring(i,j));
        if (s.charAt(i-1)=='-')
        {
            b=-b;
        }

        for (i=0,j=0;j<s1.length();j++)
        {
            if (!(s1.charAt(j)>=48&&s1.charAt(j)<=57||s1.charAt(j)==46))
            {
                break;
            }
        }
        double c = Double.parseDouble(s1.substring(i,j));

        for (i=j++;j<s1.length();j++)
        {
            if (!(s1.charAt(j)>=48&&s1.charAt(j)<=57||s1.charAt(j)==46))
            {
                break;
            }
        }
        double d = Double.parseDouble(s1.substring(i,j));
        if (s1.charAt(i-1) == '-')
        {
            d = -d;
        }

        double RealPart = a + c, ImagePart = b + d;
        String s2;
        s2 = "";
        if(ImagePart>0)
        {
            s2=RealPart+"+"+ImagePart+"i";
        }
        if(ImagePart==0)
        {
            s2=RealPart+"";
        }
        if(ImagePart<0)
        {
            s2=RealPart+""+ImagePart+"i";
        }

        writer2.append("\n"+s2);
        writer2.flush();
        System.out.print("\n计算结果为：");
        while (reader2.ready()) {
            System.out.print((char) reader2.read() + "");
        }
    }
}