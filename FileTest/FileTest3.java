import java.io.*;


public class FileTest3 {
    public static void main(String[] args) throws IOException {
        //（1）文件创建（文件类实例化）
        File file = new File("C:\\Users\\dell\\IdeaProjects\\MyUtil\\src", "HelloWorld.txt");
        //File file = new File("HelloWorld.txt");
//        File file1 = new File("C:\\Users\\besti\\Desktop\\FileTest\\Test\\Test");
//        file1.mkdir();
//        file1.mkdirs();

        if (!file.exists()) {
            file.createNewFile();
        }
        // file.delete();
        //（2）文件读写
        //第二种：字符流读写，先写后读(两种读)
        Writer writer2 = new FileWriter(file);
        writer2.write("Hello, I/O Operataion!这是利用Writer写入文件的内容");
        writer2.flush();
        writer2.append("Hello,Ly");
        writer2.flush();

        Reader reader2 = new FileReader(file);
        char[] temp = new char[100];
        reader2.read(temp);
        System.out.println();
        System.out.println(temp);
    }
}