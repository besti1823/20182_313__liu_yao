public class ASL {

    public boolean order(int[] arr,int target){
        int i=0;
        int a = target;
        while(arr[i]!=target)
        {
            i++;
            if(i==arr.length)
                break;
        }
        return i==arr.length?false:true;
    }

    public void sort(int arr[]){
        for(int i =1;i<arr.length;i++) {
            for(int j=0;j<arr.length-i;j++) {
                if(arr[j]>arr[j+1]) {
                    int temp = arr[j];

                    arr[j]=arr[j+1];

                    arr[j+1]=temp;
                }
            }
        }
    }

    public boolean binary(int[] arr,int min,int max,int mid,int target){
        boolean found = false;
        mid = (min + max) / 2;
        int midd = mid;

        if(arr[midd]==target)
            found = true;
        else if (arr[midd]!=target)
        {
            if(target<arr[midd])
            {
                max = midd-1;
                midd--;
                found = binary(arr,min,max,midd,target);
            }
            else if(target>arr[midd])
            {
                min = midd+1;
                midd++;
                found = binary(arr,min,max,midd,target);
            }
        }
        return found;
    }

    public int binaryshow(int[] arr,int min,int max,int mid,int target){
        int found = 0;
        mid = (min + max) / 2;
        int midd = mid;

        if(arr[midd]==target)
            found = arr[midd];
        else if (arr[midd]!=target)
        {
            if(target<arr[midd])
            {

                max = midd-1;
                midd--;
                found = binaryshow(arr,min,max,midd,target);
            }
            else if(target>arr[midd])
            {
                min = midd+1;
                midd++;
                found = binaryshow(arr,min,max,midd,target);
            }
        }
        return found;
    }

    public int[] hash(int[] arr){
        int[] arr1 = {0,0,0,0,0,0,0,0,0,0,0,0};
        for(int i=0;i<arr.length;i++)
        {
            if(arr1[arr[i]%11] == 0)
                arr1[arr[i]%11] = arr[i];
            else
            {
                for(int j=2;j<arr.length;j++)
                    if(arr1[j-1] == 0)
                    {
                        arr1[j-1] = arr[i];
                        break;
                    }
            }
        }
        return arr1;
    }

    public int hashsearch(int[] result,int target){
        int k = target%11,i,re = 0;
        if(result[k]==target)
            re =  result[k];
        else
        {
            for(i=k;k<result.length;k++)
            {
                if(result[k]==target)
                {
                    re = result[k];
                    break;
                }
            }
        }
        return re;
    }

    public Linked[] linkedhash(Linked[] linked){
        Linked[] arr1 = new Linked[12];
        int i;
        for(i=0;i<12;i++)
            arr1[i] = new Linked(0);
        for(i=0;i<linked.length;i++)
        {
            if((arr1[linked[i].getnum()%11]).getnum() == 0)
                arr1[linked[i].getnum()%11] = linked[i];
            else
            {
                arr1[linked[i].getnum()%11].setNext(linked[i]);
            }
        }
        return arr1;
    }

    public int linkedsearch(Linked[] re1, int target){
        int k = target%11,i,re = 0;
        if(re1[k].getnum()==target)
            re = re1[k].getnum();
        else
        {
            Linked re2 = re1[k].getNext();
            re2 = new Linked(0);
            if(re2.getnum()==target)
                re = re2.getnum();
        }
        return re;
    }

    public String print(int[] arr){
        String result = "";
        for(int i=0;i<arr.length;i++)
            result += ""+arr[i]+" ";
        return result;
    }

}