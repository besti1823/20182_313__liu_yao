import java.util.Arrays;

public class CircularArrayQueue<T> implements Queue<T> {
    private final int DEFAULT_CAPACITY = 10;
    private int f,r,c;
    private T[] q;

    public CircularArrayQueue()
    {
        f = r = c = 0;
        q = (T[]) (new Object[DEFAULT_CAPACITY]);
    }
    @Override
    public void enqueue(T element) {
        if(c==q.length)
            expandCapacity();
        q[r] =  element;
        r=(r+1)%q.length;
        c++;

    }
    private void expandCapacity() {
        T[] larger = (T[]) (new Object[q.length * 2]);
        for (int index = 0; index < c; index++)
            larger[index] = q[(f + index) % q.length];
        f = 0;
        r = c;


    }


    @Override
    public T dequeue() throws EmptyCollectionException{
        T str;
        if(f == r&&q[f]==null)
            throw new EmptyCollectionException("队列为空");
        else {
            str = q[f];
            q[f]=null;
            c--;
        }
        return str;
    }

    @Override
    public T first() throws EmptyCollectionException{

        if(f == r&&q[f]==null)
        {
            throw new EmptyCollectionException("数组为空");
        }
        else
            return  q[f];
    }

    @Override
    public boolean isEmpty() {
        if(c == 0)
            return false;
        else
            return true;
    }

    @Override
    public int size() {
        return c;
    }

    @Override
    public String toString() {
        return "CircularArrayQueue{" +
                "DEFAULT_CAPACITY=" + DEFAULT_CAPACITY +
                ", f=" + f +
                ", r=" + r +
                ", c=" + c +
                ", queue=" + Arrays.toString(q) +
                '}';
    }
}