public class Searching<T>
{
    private T[] data;
    public int mid;

    public T[] getData() {
        return data;
    }

    public void setData(T[] data) {

    }

    public static Comparable linearSearch (Comparable[] data,
                                           Comparable target)
    {
        Comparable r = null;
        int index = 0;

        while (r == null && index < data.length)
        {
            if (data[index].compareTo(target) == 0)
                r = data[index];
            index++;
        }

        return r;
    }

    public static <T extends Comparable<T>>
    boolean binarySearch (Comparable[] data, int min, int max,int mid,Comparable target)
    {
        boolean f = false;
        //int mid = (min + max) / 2;

        if(data[mid].compareTo(target)==0)
            f= true;
        else if (data[mid].compareTo(target)>0)
        {
            if(min<mid-1)
            {
                mid--;
            }
        }
        else if(mid+1<=max)
        {
            mid++;
            f = binarySearch(data,min,max,mid,target);
        }
        return f;
    }

    public static <T extends Comparable<T>>
    Comparable binaryShow (Comparable[] data, int min, int max,int mid,Comparable target)
    {
        Comparable f = null;
        //int mid = (min + max) / 2;

        if(data[mid].compareTo(target)==0)
            f = data[mid];
        else if (data[mid].compareTo(target)>0)
        {
            if(min<mid-1)
            {
                mid--;
                f = binaryShow(data,min,max,mid,target);
            }
        }
        else if(mid+1<=max)
        {
            mid++;
            f = binaryShow(data,min,max,mid,target);
        }
        return f;
    }
}