public class HuffmanNode implements Comparable<HuffmanNode>{
    protected  String name;
    protected double length;
    protected HuffmanNode left;
    protected HuffmanNode right;
    protected String code;

    public HuffmanNode(String name, double weight){
        this.name = name;
        this.length = weight;
        code = "";
    }

    public String toString() {
        return "name: " + name + " length: " + length + " ����Ϊ: " + code +"\n";
    }

    public int compareTo(HuffmanNode node) {
        if (length >= node.length){
            return 1;
        }
        else {
            return -1;
        }

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getLength() {
        return length;
    }

    public void setLength(double length) {
        this.length = length;
    }

    public HuffmanNode getLeft() {
        return left;
    }

    public void setLeft(HuffmanNode left) {
        this.left = left;
    }

    public HuffmanNode getRight() {
        return right;
    }

    public void setRight(HuffmanNode right) {
        this.right = right;
    }

    public String getCode(){
        return code;
    }

    public void setCode(String str){
        code = str;
    }

}
