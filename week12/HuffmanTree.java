import java.io.*;
import java.text.DecimalFormat;
import java.util.*;

public class HuffmanTree {
    public static HuffmanNode createTree(List<HuffmanNode> nodes) {
        while (nodes.size() > 1) {
            Collections.sort(nodes);

            HuffmanNode left = nodes.get(nodes.size() - 2);
            left.setCode("0");
            HuffmanNode right = nodes.get(nodes.size() - 1);
            right.setCode("1");
            HuffmanNode parent = new HuffmanNode(null, left.getLength() + right.getLength());
            parent.setLeft(left);
            parent.setRight(right);
            nodes.remove(left);
            nodes.remove(right);
            nodes.add(parent);
        }
        return nodes.get(0);
    }

    public List<HuffmanNode> breadth(HuffmanNode root) {
        List<HuffmanNode> list = new ArrayList<HuffmanNode>();
        Queue<HuffmanNode> queue = new ArrayDeque<HuffmanNode>();

        if (root != null) {
            queue.offer(root);
            root.getLeft().setCode(root.getCode() + "0");
            root.getRight().setCode(root.getCode() + "1");
        }

        while (!queue.isEmpty()) {
            list.add(queue.peek());
            HuffmanNode node = queue.poll();
            if (node.getLeft() != null)
                node.getLeft().setCode(node.getCode() + "0");
            if (node.getRight() != null)
                node.getRight().setCode(node.getCode() + "1");

            if (node.getLeft() != null) {
                queue.offer(node.getLeft());
            }

            if (node.getRight() != null) {
                queue.offer(node.getRight());
            }
        }
        return list;
    }

    public static void main(String[] args) throws IOException {


        List<String> list = new ArrayList<String>();
        list.add("a");
        list.add("b");
        list.add("c");
        list.add("d");
        list.add("e");
        list.add("f");
        list.add("g");
        list.add("h");
        list.add("i");
        list.add("j");
        list.add("k");
        list.add("l");
        list.add("m");
        list.add("n");
        list.add("o");
        list.add("p");
        list.add("q");
        list.add("r");
        list.add("s");
        list.add("t");
        list.add("u");
        list.add("v");
        list.add("w");
        list.add("x");
        list.add("y");
        list.add("z");
        list.add(" ");
        int[] number = new int[]{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
        File file = new File("D:\\text.txt");
        if (!file.exists()) {
            file.createNewFile();
        }
        BufferedReader br = new BufferedReader(new FileReader(file));
        String s;
        String message = "";
        while((s = br.readLine()) != null){
            message += s;
        }
        String[] result = message.split("");
        for (int n = 0;n < result.length; n++){
            for (int i = 0; i < 27; i++){
                if (result[n].equals(list.get(i))){
                    number[i] += 1;
                }
            }
        }
        List<HuffmanNode> nodeList = new ArrayList<HuffmanNode>();
        DecimalFormat df = new DecimalFormat( "0.0000000");
        double wei;
        double sum = result.length;
        for(int i = 0;i<27;i++){
            wei = ((double) number[i]/sum);
            System.out.println(list.get(i) + "出现" + number[i] + "次,概率为" + df.format(wei));
            nodeList.add(new HuffmanNode(list.get(i),number[i]));
        }
        Collections.sort(nodeList);
        HuffmanTree huffmanTree = new HuffmanTree();
        HuffmanNode node = huffmanTree.createTree(nodeList);
        List<HuffmanNode> inlist = new ArrayList<HuffmanNode>();
        inlist = huffmanTree.breadth(node);

        String[] name = new String[number.length];
        String[] code = new String[number.length];

        File file1 = new File("编码文件.txt");
        File file2 = new File("解码文件.txt");
        FileWriter fileWriter1 = new FileWriter(file1);
        FileWriter fileWriter2 = new FileWriter(file2);


        int temp = 0;
        for(int e = 0;e<inlist.size();e++){
            if(inlist.get(e).getName() != null){
                System.out.println(inlist.get(e).getName()+"的编码为"+ inlist.get(e).getCode()+" ");
                name[temp] = inlist.get(e).getName();
                code[temp] = inlist.get(e).getCode();
                temp++;
            }
        }

        String res = "";
        for(int f = 0; f < sum; f++){
            for(int j = 0;j<name.length;j++){
                if(message.charAt(f) == name[j].charAt(0))
                    res += code[j];
            }
        }

        System.out.println("编码后："+ res);

        List<String> putlist = new ArrayList<String>();
        for(int i = 0;i < res.length();i++){
            putlist.add(res.charAt(i)+"");
        }


        String string1 = "";
        String string2 = "";
        for(int h = putlist.size(); h > 0; h--){
            string1 = string1 + putlist.get(0);
            putlist.remove(0);
            for(int i=0;i<code.length;i++){
                if (string1.equals(code[i])) {
                    string2 = string2+""+ name[i];
                    string1 = "";
                }
            }
        }
        System.out.println("解码后：" + string2);

        fileWriter1.write(res);
        fileWriter2.write(string2);
        fileWriter1.close();
        fileWriter2.close();
    }


    private static int getFileLineCount(File file) {
        int cnt = 0;
        InputStream is = null;
        try {
            is = new BufferedInputStream(new FileInputStream(file));
            byte[] c = new byte[1024];
            int readChars = 0;
            while ((readChars = is.read(c)) != -1) {
                for (int i = 0; i < readChars; ++i) {
                    if (c[i] == '\n') {
                        ++cnt;
                    }
                }
            }
        } catch (Exception ex) {
            cnt = -1;
            ex.printStackTrace();
        } finally {
            try {
                is.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        return cnt;
    }


}
