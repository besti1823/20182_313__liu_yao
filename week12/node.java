public class node {
    public node next;
    public int ele;
    public node next2;
    public node(int a){
        next=null;
        ele=a;
        next2=null;
    }
    public void setNext(node temp){
        next=temp;
    }
    public node getNext(){
        return next;
    }

    public int getEle() {
        return ele;
    }

    public void setEle(int ele) {
        this.ele = ele;
    }

    public node getNext2() {
        return next2;
    }

    public void setNext2(node next2) {
        this.next2 = next2;
    }
}