import junit.framework.TestCase;
import org.junit.Test;

public class StringBufferDemoTest extends TestCase {
    StringBuffer a = new StringBuffer("StringBuffer");
    StringBuffer b = new StringBuffer("StringBufferStringBuffer");
    StringBuffer c = new StringBuffer("StringBufferStringBufferStringBufferStringBuffer");
    @Test
    public void testcharAt() throws Exception {
        assertEquals('S',a.charAt(0));
        assertEquals('B',b.charAt(6));
        assertEquals('i',c.charAt(15));
    }
    @Test
    public void testcapacity() throws Exception {
        assertEquals(28,a.capacity());
        assertEquals(40,b.capacity());
        assertEquals(64,c.capacity());
    }
    @Test
    public void testlength() throws Exception {
        assertEquals(12,a.length());
        assertEquals(24,b.length());
        assertEquals(48,c.length());
    }
    @Test
    public void testindexOf() throws Exception {
        assertEquals(3,a.indexOf("ing"));
        assertEquals(11,b.indexOf("rSt"));
        assertEquals(-1,c.indexOf("abca"));
    }
}