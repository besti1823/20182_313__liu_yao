public class Cat extends Animal {

    public Cat(String name, String id) {
        super(name, id);
    }

    @Override
    public void feed() {
        System.out.println("主人请赐予我鱼");
    }
}
