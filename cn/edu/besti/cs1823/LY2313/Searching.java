package cn.edu.besti.cs1823.LY2313;

public class Searching {
    public static Comparable linearSearch(Comparable[] data, Comparable target) {
        //线性查找
        Comparable result = null;
        int index = 0;

        while (result == null && index < data.length) {
            if (data[index].compareTo(target) == 0)
                result = data[index];
            index++;
        }

        return result;
    }

    public static Comparable binarySearch(Comparable[] data1, Comparable target) {
        //折半查找
        //复制一个数组，避免改变原数组的顺序
        Comparable[] data = data1;
        Sorting.bubbleSort(data);

        Comparable result = null;
        int first = 0, last = data.length - 1, mid;
        while (result == null && first <= last) {
            mid = (first + last) / 2;
            if (data[mid].compareTo(target) == 0)
                result = data[mid];
            else if (data[mid].compareTo(target) > 0)
                last = mid - 1;
            else
                first = mid - 1;
        }

        return result;
    }

    public static int insertionSearch(int a[], int value, int low, int high) {
        //插值查找
        //基于二分查找算法，将查找点的选择改进为自适应选择
        int mid = low+(value-a[low])/(a[high]-a[low])*(high-low);
        if(a[mid]==value)
            return mid;
        else if(a[mid]>value)
            return insertionSearch(a, value, low, mid-1);
        else
            return insertionSearch(a, value, mid+1, high);
    }



    /*构造一个斐波那契数组*/
    private static void fibonacci(int[] F)
    {
        final int MAX_SIZE=20;//斐波那契数组的长度
        F[0]=0;
        F[1]=1;
        for(int i=2;i<MAX_SIZE;++i)
            F[i]=F[i-1]+F[i-2];
    }

    /*定义斐波那契查找法*/
    public static int fibonacciSearch(int[] a, int n, int key)  //a为要查找的数组,n为要查找的数组长度,key为要查找的关键字
    {
        final int MAX_SIZE=20;//斐波那契数组的长度
        int low=0;
        int high=n-1;

        int[] F= new int[MAX_SIZE];
        fibonacci(F);//构造一个斐波那契数组F

        int k=0;
        while(n>F[k]-1)//计算n位于斐波那契数列的位置
            ++k;

        int[] temp;//将数组a扩展到F[k]-1的长度
        temp=new int [F[k]-1];
        for(int i = 0; i < F[k]-1;i++){
            temp[i] = a[i];
        }

        for(int i=n;i<F[k]-1;++i)
            temp[i]=a[n-1];

        while(low<=high)
        {
            int mid=low+F[k-1]-1;
            if(key<temp[mid])
            {
                high=mid-1;
                k-=1;
            }
            else if(key>temp[mid])
            {
                low=mid+1;
                k-=2;
            }
            else
            {
                if(mid<n)
                    return mid; //若相等则说明mid即为查找到的位置
                else
                    return n-1; //若mid>=n则说明是扩展的数值,返回n-1
            }
        }

        return -1;
    }
}