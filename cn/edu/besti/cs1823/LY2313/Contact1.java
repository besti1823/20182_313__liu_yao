package cn.edu.besti.cs1823.LY2313;

public class Contact1 implements Comparable
{
    private String firstName, lastName, phone;

    //-----------------------------------------------------------------
    //  Sets up this contact with the specified information.
    //-----------------------------------------------------------------
    public Contact1 (String first, String last, String telephone)
    {
        firstName = first;
        lastName = last;
        phone = telephone;
    }

    //-----------------------------------------------------------------
    //  Returns a string representation of this contact.
    //-----------------------------------------------------------------
    public String toString ()
    {
        return lastName + ", " + firstName + ":  " + phone;
    }

    public int compareTo (Object other)
    {
        int result;
        result = phone.compareTo(((Contact1)other).phone);
        return result;
    }

    public static void select(Contact1[] a){
        Contact1 temp;
        int mix;
        for(int i=0;i<a.length-1;i++) //每次循环数组，找出最小的元素，放在前面，前面的即为排序好的
        {
            mix=i; //假设最小元素的下标
            for(int j=i+1;j<a.length;j++) //将上面假设的最小元素与数组比较，交换出最小的元素的下标
                if(a[j].phone.compareTo(a[mix].phone)>0)
                    mix=j;
            //若数组中真的有比假设的元素还小，就交换
            if(i!=mix)
            {
                temp=a[i];
                a[i]=a[mix];
                a[mix]=temp;
            }
        }
        for(int m=0;m<a.length;m++)
            System.out.println(a[m]);
    }
}