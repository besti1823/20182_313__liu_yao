package cn.edu.besti.cs1823.LY2313;

import java.util.ArrayList;
import java.util.List;

public class Sorting {
    private static void swap (Comparable[] data, int index1, int index2){
        //交换数组的两个值
        Comparable temp = data[index1];
        data[index1] = data[index2];
        data[index2] = temp;
    }

    public static void SelectionSort(Comparable[] data){
        //从小到大，选择排序
        int min;

        for(int index = 0; index < data.length - 1; index++){
            min = index;
            for(int scan = index+1; scan < data.length; scan++)
                if(data[scan].compareTo(data[min]) > 0)
                    min = scan;

            swap(data, min, index);
        }
    }

    public static void bubbleSort(Comparable[] data){
        //从小到大，（冒泡）排序
        int position,scan;

        for(position = data.length-1; position >= 0; position--){
            for(scan = 0; scan <= position -1; scan++)
                if(data[scan].compareTo(data[scan+1]) > 0)
                    swap(data, scan, scan+1);
        }
    }

    public static void insertionSort(Comparable[] data){
        for (int index = 1; index < data.length; index++){
            Comparable key = data[index];
            int position = index;

            while (position > 0 && data[position - 1].compareTo(key) > 0){
                data[position] = data[position - 1];
                position--;
            }
            data[position] = key;
        }
    }

    public static void quickSort(Comparable[] data, int min, int max){
        int pivot;

        if (min < max){
            pivot = partition(data, min, max);
            quickSort(data, min, pivot - 1);
            quickSort(data, pivot + 1, max);
        }
    }

    private static int partition (Comparable[] data, int min, int max){
        Comparable partitionValue = data[min];

        int left = min;
        int right = max;

        while (left < right){
            while (data[left].compareTo(partitionValue) <= 0 && left < right)
                left++;
            while (data[right-1].compareTo(partitionValue) > 0)
                right++;
            if (left < right)
                swap(data, left, right);
        }
        swap(data, min, right);

        return right;
    }

    public static void mergeSort (Comparable[] data, int min, int max){
        if (min < max){
            int mid = (min + max) / 2;
            mergeSort(data, min, mid);
            mergeSort(data, mid+1, max);
            merge (data, min, mid, max);
        }
    }

    public static void merge(Comparable[] data, int first, int mid, int last) {
        Comparable[] temp = new Comparable[data.length];

        int first1 = first, last1 = mid;
        int first2 = mid + 1, last2 = last;
        int index = first1;

        while (first1 <= last1 && first2 <= last2){
            if (data[first1].compareTo(data[first2]) < 0){
                temp[index] = data[first1];
                first1++;
            }
            else {
                temp[index] = data[first2];
                first2++;
            }
            index++;
        }

        while (first1 <= last1){
            temp[index] = data[first1];
            first1++;
            index++;
        }

        while (first2 <= last2){
            temp[index] = data[first2];
            first2++;
            index++;
        }

        for (index = first; index <= last; index++){
            data[index] = temp[index];
        }
    }

    public static void shellSort(int[] arr){
        //inc是增量
        int temp = 0;
        int j = 0;
        //增量默认是长度的一半，每次变为之前的一半，直到最终数组有序
        for(int inc=arr.length/2 ; inc>=1 ; inc/=2){
            for(int i=inc ; i<arr.length; i++){
                temp = arr[i];
                //将当前的数与减去增量之后位置的数进行比较，如果大于当前数，将他后移
                for(j=i-inc; j>=0;j-=inc){
                    if(arr[j]>temp){
                        arr[j+inc] = arr[j];
                    }else{
                        break;
                    }
                }
                //将当前数放到空出来的位置
                arr[j+inc]=temp;
            }
        }
    }

    public static void heapSort(int []arr){
        //1.构建大顶堆
        for(int i=arr.length/2-1;i>=0;i--){
            //从第一个非叶子结点从下至上，从右至左调整结构
            adjustHeap(arr,i,arr.length);
        }
        //2.调整堆结构+交换堆顶元素与末尾元素
        for(int j=arr.length-1;j>0;j--){
            swap(arr,0,j);//将堆顶元素与末尾元素进行交换
            adjustHeap(arr,0,j);//重新对堆进行调整
        }

    }

    /**
     * 调整大顶堆（仅是调整过程，建立在大顶堆已构建的基础上）
     * @param arr
     * @param i
     * @param length
     */
    private static void adjustHeap(int []arr,int i,int length){
        int temp = arr[i];//先取出当前元素i
        for(int k=i*2+1;k<length;k=k*2+1){//从i结点的左子结点开始，也就是2i+1处开始
            if(k+1<length && arr[k]<arr[k+1]){//如果左子结点小于右子结点，k指向右子结点
                k++;
            }
            if(arr[k] >temp){//如果子节点大于父节点，将子节点值赋给父节点（不用进行交换）
                arr[i] = arr[k];
                i = k;
            }else{
                break;
            }
        }
        arr[i] = temp;//将temp值放到最终的位置
    }

    private static void swap(int []arr,int a ,int b){
        int temp=arr[a];
        arr[a] = arr[b];
        arr[b] = temp;
    }


    public static class Node {
        //左节点
        public Node leftNode;
        // 右子节点
        public Node rightNode;

        // 值
        public Object value;

        // 插入 数据的方法
        public void add(Object v) {
            // 如果当前节点没有值，就把数据放在当前节点上
            if (null == value)
                value = v;

                // 如果当前节点有值，就进行判断，新增的值与当前值的大小关系
            else {
                // 新增的值，比当前值小或者相同

                if ((Integer) v - ((Integer) value) <= 0) {
                    if (null == leftNode)
                        leftNode = new Node();
                    leftNode.add(v);
                }
                // 新增的值，比当前值大
                else {
                    if (null == rightNode)
                        rightNode = new Node();
                    rightNode.add(v);
                }

            }

        }

        // 中序遍历所有的节点
        public List<Object> values() {
            List<Object> values = new ArrayList<>();

            // 左节点的遍历结果
            if (null != leftNode)
                values.addAll(leftNode.values());

            // 当前节点
            values.add(value);

            // 右节点的遍历结果
            if (null != rightNode)

                values.addAll(rightNode.values());

            return values;
        }
    }
}