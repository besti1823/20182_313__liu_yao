import java.util.LinkedList;
import java.util.Queue;
import java.util.Stack;

class Node1<T> {
    T val;
    Node1 left;
    Node1 right;

    public Node1() {
    }

    public Node1(T val) {
        this.val = val;
    }

    public Node1(T val, Node1 left, Node1 right) {
        this.val = val;
        this.left = left;
        this.right = right;
    }
}
//AB#CD###E#F##
public class Tree {
    private static char[] arr = {'A', 'B', '#', 'C', 'D', '#', '#', '#', 'E', '#', 'F', '#', '#'};
    private static int count = 0;

    //构建二叉树(#代表空结点)

    public static Node1 Create() {
        if (count >= arr.length || arr[count] == '#') {
            count++;// 跳过空结点
            return null;        }
        Node1 node1 = new Node1<>(arr[count++]);
        node1.left = Create();
        node1.right = Create();
        return node1;
    }

    //先序遍历

    public static void preOrder(Node1 root) {
        if (root != null) {
            System.out.print(root.val + " ");
            preOrder(root.left);
            preOrder(root.right);
        }
    }

    //中序遍历

    public static void inOrder(Node1 root) {
        if (root != null) {
            inOrder(root.left);
            System.out.print(root.val + " ");
            inOrder(root.right);
        }
    }

    //后序遍历

    public static void postOrder(Node1 root) {
        if (root != null) {
            postOrder(root.left);
            postOrder(root.right);
            System.out.print(root.val + " ");
        }
    }

    //层次遍历


    public static void levOrder(Node1 root) {
        if (root != null) {
            Node1 p = root;
            Queue<Node1> queue = new LinkedList<>();
            queue.add(p);
            while (!queue.isEmpty()) {
                p = queue.poll();
                System.out.print(p.val + " ");
                if (p.left != null) {
                    queue.add(p.left);
                }
                if (p.right != null) {
                    queue.add(p.right);
                }
            }
        }
    }

    //非递归前序
    public static void preOrderTraverse(Node1 root) {
        Stack<Node1> stack = new Stack<>();
        Node1 node1 = root;
        while (node1 != null || !stack.empty()) {
            if (node1 != null) {
                System.out.print(node1.val + " ");
                stack.push(node1);
                node1 = node1.left;
            } else {
                Node1 tem = stack.pop();
                node1 = tem.right;
            }
        }
    }

    //非递归中序
    public static void inOrderTraverse(Node1 root) {
        Stack<Node1> stack = new Stack<>();
        Node1 node1 = root;
        while (node1 != null || !stack.isEmpty()) {
            if (node1 != null) {
                stack.push(node1);
                node1 = node1.left;
            } else {
                Node1 tem = stack.pop();
                System.out.print(tem.val + " ");
                node1 = tem.right;
            }
        }
    }

    //计算叶子节点的个数

    public static int getSize(Node1 root) {
        if (root == null) {
            return 0;
        }
        if (root.left == null && root.right == null) {
            return 1;
        } else {
            return getSize(root.left) + getSize(root.right);
        }
    }

    //计算二叉树的高度

    public static int getHeight(Node1 root) {
        if (root == null) {
            return 0;
        }
        int leftHeight = getHeight(root.left);
        int rightHeight = getHeight(root.right);
        return (leftHeight > rightHeight) ? (leftHeight + 1) : (rightHeight + 1);
    }

    public static void main(String[] args) {
        System.out.println("\n输入为：AB#CD###E#F##");

        Node1 root = Create();
        System.out.println("\n递归先序遍历：");
        preOrder(root);

        System.out.println("\n递归中序遍历：");
        inOrder(root);

        System.out.println("\n递归后序遍历：");
        postOrder(root);

        System.out.println("\n非递归先序遍历：");
        preOrderTraverse(root);

        System.out.println("\n非递归中序遍历：");
        inOrderTraverse(root);

        System.out.println("\n层次遍历：");
        levOrder(root);

        System.out.println("\n二叉树叶子结点数：" + getSize(root));
        System.out.println("二叉树高度：" + getHeight(root));
    }
}