package cn.edu.besti.cs1823.LY2313;

public class Test1 {
    public static void main(String[] args) {
        Contact1[] a = new Contact1[3];
        Contact1[] b = new Contact1[3];
        a[0] = new Contact1 ("g", "a", "0218");
        a[1] = new Contact1 ("o", "k", "1999");
        a[2] = new Contact1 ("u", "a", "2018");

        b[0] = new Contact1 ("n", "r", "2331");
        b[1] = new Contact1 ("a", "n", "1056");
        b[2] = new Contact1 ("g", "l", "0505");

        Contact1 target1 = new Contact1("","","0505");//小边界
        Contact1 target2 = new Contact1("","","0000");//异常
        Contact1 target3 = new Contact1("","","2331");//大边界
        Contact1 target4 = new Contact1("","","1989");//异常
        Contact1 target5 = new Contact1("","","0894");//正常

        Contact1 c[] = new Contact1[10];
        c[0] = (Contact1)Searching.linearSearch(b,target1);
        c[1] = (Contact1)Searching.linearSearch(b,target2);
        c[2] = (Contact1)Searching.linearSearch(b,target3);
        c[3] = (Contact1)Searching.linearSearch(a,target4);
        c[4] = (Contact1)Searching.linearSearch(a,target5);
        c[5] = (Contact1)Searching.linearSearch(b,target5);
        c[6] = (Contact1)Searching.linearSearch(a,target1);
        c[7] = (Contact1)Searching.linearSearch(a,target3);

        for(int i=0;i<8;i++){
            System.out.println("Test"+(i+1)+":");
            if(c[i] == null)
                System.out.println("b was not found.");
            else
                System.out.println("c:  "+ c[i]);
        }

        Sorting.selectionSort(b);//倒序
        Sorting.selectionSort(a);//正序

        System.out.println("Test9:");
        for(Comparable play :b)
            System.out.println(play);
        System.out.println("Test10:");
        for(Comparable play :a)
            System.out.println(play);


    }
}